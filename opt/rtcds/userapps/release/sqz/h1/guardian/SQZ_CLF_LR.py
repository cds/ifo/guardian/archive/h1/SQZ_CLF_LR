# Nutsinee Kijbunchoo Aug 28, 2018
#
# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_CLF_LR.py $
import sys
import time
from guardian import GuardState, GuardStateDecorator

import sqzparams

nominal = 'LOCKED'
request = 'DOWN'

use_clf_iss = sqzparams.use_clf_iss

#############################################
#Functions

def CLF_locked():
    if ezca['SQZ-CLF_SERVO_COMEXCEN'] or ezca['SQZ-CLF_SERVO_FASTEXCEN']:
        notify('CMB EXC is ON')
    return abs(ezca['SQZ-CLF_VCXO_CONTROLS_DIFFFREQUENCY']) < 20

def CLF_railing():
    flag = False
    if (abs(ezca['SQZ-CLF_SERVO_FASTMON']) > 9.5):
        notify('CLF FASTMON railing!')
        flag = True
    return flag

def CLF_ISS_locked():
    return (ezca['SQZ-CLF_ISS_OUTPUTRAMP']==0 and ezca['SQZ-CLF_ISS_CONTROLMON']>0)

def lock_CLF_ISS():
    ezca['SQZ-CLF_ISS_OUTPUTRAMP'] = 0  #enable CLF ISS servo ("servo")
    ezca['SQZ-CLF_ISS_BOOST1'] = 1
    #time.sleep(0.5)
    #ezca['SQZ-CLF_ISS_BOOST2'] = 1
    #time.sleep(0.5)
    return True

def down_CLF_ISS():
    #ezca['SQZ-CLF_ISS_BOOST2'] = 0
    ezca['SQZ-CLF_ISS_BOOST1'] = 0
    ezca['SQZ-CLF_ISS_OUTPUTRAMP'] = 1  #disable CLF ISS servo ("ramp")
    return True

#############################################
class INIT(GuardState):
    index = 0

    def main(self):
        #reset clf iss settings
        ezca['SQZ-CLF_ISS_IN1EN'] = 1
        ezca['SQZ-CLF_ISS_POLARITY'] = 1
        ezca['SQZ-CLF_ISS_GAIN'] = 23

        if CLF_locked():
            return 'LOCKED'
        else:
            return 'DOWN'


class IDLE(GuardState):
    index = 2
    request = False

    def run(self):
        notify('6MHz RF Mon low or non. Check shutter, check CLF.')
        return ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] > -40

class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SQZ-CLF_SERVO_IN2EN'] = 0
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        ezca['SQZ-CLF_SERVO_FASTEN'] = 1

        down_CLF_ISS() #bring CLF ISS loop down
        ezca['SQZ-CLF_ISS_LIMITRESET'] = 1  # reset CLF ISS lockloss counter

    def run(self):
        return True

class LOCKING(GuardState):
    index = 5
    request = False

    def main(self):
        if ezca['SYS-MOTION_C_SHUTTER_I_STATE']:
            notify('Opening CLF TRIG shutter!')
            ezca['SYS-MOTION_C_SHUTTER_I_OPEN'] = 1

        self.counter = -1

    def run(self):
        if CLF_railing():
            return 'DOWN'

        if ezca['GRD-SQZ_OPO_LR_STATE_N'] < 17:
            notify('waiting for OPO to LOCKED_CLF_DUAL')
            return False

        if self.counter == -1:
            if not ezca['SQZ-CLF_FLIPPER_READBACK']: #open HD flipper when locking LO on HD
                notify('CLF FLIPPER CLOSED ON SQZT0!')
            else:
                self.counter += 1

        if ezca['SYS-MOTION_C_SHUTTER_I_STATE']:
            notify('NO CLF RF6 b/c CLF TRIG shutter closed on SQZT7!')
            return False

        if self.counter == 0:
        # VX 12/15 enable CLF_ISS, D.Sigg LHO:66393 installed 105kHz clf_iss notches
            if use_clf_iss:
            	lock_CLF_ISS()
            	time.sleep(0.5)
            self.counter += 1

        if self.counter == 1:
            if ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] < -50:
                notify('6MHz too low? Noise floor at -55.')
            else:
                ezca['SQZ-CLF_SERVO_IN2EN'] = 1
                self.timer['pause']=1
                self.counter += 1

        if self.counter == 2 and self.timer['pause']:
            log('checking clf diff freq to boost')
            if CLF_locked():
                ezca['SQZ-CLF_SERVO_COMBOOST'] = 3
                return True

        if use_clf_iss and not CLF_ISS_locked(): # added for diagnostics
            notify('no CLF ISS')


class LOCKED(GuardState):
    index = 10

    def run(self):
        if not CLF_locked():
            return 'DOWN'

        if ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] < -50:
            notify('RF6 too low!! Noise floor at -55.')

        if CLF_railing():
            return 'DOWN'

        if use_clf_iss and not CLF_ISS_locked():
            notify('CLF ISS railing!')
            if ezca['SQZ-CLF_ISS_LIMITCOUNT'] > 30 or ezca['SQZ-CLF_ISS_CONTROLMON']<0:
                down_CLF_ISS()
                notify('Disabling CLF_ISS loop after 30 locklosses')

        return True


#############################################

edges = [
    ('INIT', 'IDLE'),
    ('INIT', 'DOWN'),
    ('LOCKED', 'IDLE'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'LOCKING'),
    ('LOCKING', 'LOCKED')
]


